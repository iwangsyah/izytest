import * as types from '../constants/action-types'

export function issuesUpdate(issues) {
  return {
    type: types.ISSUES_UPDATE,
    issues: issues
  }
}
