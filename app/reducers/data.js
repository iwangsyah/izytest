import _ from 'lodash'

import * as types from '../constants/action-types'

export default function setIssues(state, action) {
  state = state || {
    issues: [
              {
                id: 1,
                name: "AC Issue"
              },
              {
                id: 2,
                name: "Bathroom Issue"
              },
              {
                id: 3,
                name: "TV Issue"
              }
          ]
  }

  switch (action.type) {
    case types.ISSUES_UPDATE:
      state = _.assign({}, state, { issues: action.issues })
      return state
    default:
      return state
  }
}
