import _ from 'lodash'
import { combineReducers } from 'redux'

import * as types from '../constants/action-types'

import data from './data'

const appReducer = combineReducers({
  data
})

const rootReducer = (state, action) => {
  return appReducer(state, action)
}

export default rootReducer
