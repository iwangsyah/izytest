import React, { Component } from 'react';
import {
  FlatList,
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import navbarStyles from '../styles/navbar'
import { BackIcon, GoIcon } from '../lib/images'

class ListingIssues extends Component {
  constructor(props) {
    super(props)
    this.state = {
      result: props.issues
    }
    this.renderRow = this.renderRow.bind(this)
  }

  renderHeader() {
    return (
      <View style={[navbarStyles.containerNavbar, {flexDirection: 'row', justifyContent: 'space-between'}]}>
        <TouchableOpacity style={{padding:20, paddingLeft:0}} onPress={() => Actions.scannerSuccess()}>
          <BackIcon/>
        </TouchableOpacity>
        <Text style={navbarStyles.textNavbar}>List Issues</Text>
        <View style={{width:30}} />
      </View>
    )
  }

  renderRow(result) {
    return (
       <TouchableOpacity
          onPress={() => Actions.editIssue({ type:'reset', item: result.item })}
          style={{ flex:1, paddingVertical: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: 'grey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}
       >
        <View>
           <Text style={{color:'#000', fontSize: 16}}>{result.item.name}</Text>
           <Text style={{fontSize: 12, marginLeft: 10}}>{result.item.notes}</Text>
         </View>
         <GoIcon/>
       </TouchableOpacity>
    )
  }

  keyExtractor(data) {
    if (data) {
      return data.id
    }
  }

  render() {
    let { result } = this.state
    let data = []

    let content = null
    content = (
        <FlatList
          data = {data.concat(this.state.result)}
          style={{paddingHorizontal: 15}}
          keyExtractor = {this.keyExtractor}
          renderItem = {this.renderRow} />
    )

    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        {this.renderHeader()}
        {content}
      </View>
    )
  }
}

let mapStateToProps = (state) => {
  
  return {
    issues: state.data.issues
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListingIssues)
