import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import _ from 'lodash';

import { issuesUpdate } from '../actions'
import navbarStyles from '../styles/navbar'
import { BackIcon } from '../lib/images'

class EditIssue extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notes : props.item.notes || '',
      check_in : props.item.check_in || '',
      check_out : props.item.check_out || '',
    }
  }

  renderHeader() {
    return (
      <View style={[navbarStyles.containerNavbar, {flexDirection: 'row', justifyContent: 'space-between'}]}>
        <TouchableOpacity style={{padding:20, paddingLeft:0}} onPress={() => Actions.pop()}>
          <BackIcon/>
        </TouchableOpacity>
        <Text style={navbarStyles.textNavbar}>{this.props.item.name}</Text>
        <View style={{width:30}} />
      </View>
    )
  }

  submit() {
    let { notes, check_in, check_out } = this.state
    let { issues, item, issuesUpdate } = this.props
    objIndex = issues.findIndex((obj => obj.id == item.id));

    issues[objIndex].notes = notes
    issues[objIndex].check_in = check_in
    issues[objIndex].check_out = check_out

    issuesUpdate(issues)
    Actions.listingIssues()
  }

  render() {
    let { notes, check_in, check_out } = this.state
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        {this.renderHeader()}
        <View style={{paddingHorizontal: 15}}>

          <View>
            <Text>Notes</Text>
            <TextInput
              style={{paddingLeft: 10, marginBottom: 20}}
              onChangeText={(notes) => this.setState({notes})}
              value={notes}
            />
          </View>

          <View>
            <Text>Check In Time (UTC)</Text>
            <TextInput
              style={{paddingLeft: 10, marginBottom: 20}}
              onChangeText={(check_in) => this.setState({check_in})}
              value={check_in}
              keyboardType='numeric'
            />
          </View>

          <View>
            <Text>Check Out Time (UTC)</Text>
            <TextInput
              style={{paddingLeft: 10}}
              onChangeText={(check_out) => this.setState({check_out})}
              value={check_out}
              keyboardType='numeric'
            />
          </View>

          <TouchableOpacity style={styles.containerSubmit} onPress={this.submit.bind(this)}>
            <Text style={{color:'#fff', fontSize:20}}>Save</Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}

let mapStateToProps = (state, props) => {
  return {
    issues: state.data.issues
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    issuesUpdate: (issues) => {
      dispatch(issuesUpdate(issues))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditIssue)


const styles = StyleSheet.create({
  containerSubmit: {
    backgroundColor: '#42c3f4',
    width: 100,
    paddingVertical:10,
    marginTop: 30,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius:5
  }
})
