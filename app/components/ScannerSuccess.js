import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import navbarStyles from '../styles/navbar'
import boxContentStyles from '../styles/boxContent'
import { GoIcon } from '../lib/images'

export default class ScannerSuccessScreen extends Component {
  renderHeader() {
    return (
      <View style={navbarStyles.containerNavbar}>
        <Text style={navbarStyles.textNavbar}>Data Scanner Details</Text>
      </View>
    )
  }

  render() {
    let { data } = this.props
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        <View style={{paddingHorizontal: 15}}>
          <View style={boxContentStyles.boxContent}>
            <View style={boxContentStyles.boxContentText}>
              <Text style={boxContentStyles.textTitle}>Name</Text>
              <Text style={boxContentStyles.text}>{data.name}</Text>
            </View>
          </View>
          <View style={boxContentStyles.boxContent}>
            <View style={boxContentStyles.boxContentText}>
              <Text style={boxContentStyles.textTitle}>Check In Time (UTC)</Text>
              <Text style={boxContentStyles.text}>{data.check_in_time}</Text>
            </View>
          </View>
          <View style={boxContentStyles.boxContent}>
            <View style={boxContentStyles.boxContentText}>
              <Text style={boxContentStyles.textTitle}>Check Out Time (UTC)</Text>
              <Text style={boxContentStyles.text}>{data.check_out_time}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => Actions.listingIssues()} style={boxContentStyles.boxContent}>
            <View style={boxContentStyles.boxContentText}>
              <Text style={boxContentStyles.textTitle}>Go To List Issues</Text>
            </View>
            <GoIcon/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles =  StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  boxContentLogout: {
    flexDirection:'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: 'rgb(220,220,220)',
    paddingVertical: 17,
    width: '95%'
  }
})
