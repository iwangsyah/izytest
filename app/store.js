import { AsyncStorage } from 'react-native'
import { compose, createStore, applyMiddleware } from 'redux'
import { persistStore, autoRehydrate } from 'redux-persist'
import { createWhitelistFilter } from 'redux-persist-transform-filter'
import thunk from 'redux-thunk'

import { enableBatching } from './lib/redux-batched-actions'
import logger from './middlewares/logger'
import logActions from './middlewares/logActions'
import reducer from './reducers/'
// import { deviceSetOrientation } from './actions/device'

const store = createStore(
  enableBatching(reducer),
  compose(
    applyMiddleware(thunk, logActions),
    //applyMiddleware(thunk, logger),
    //autoRehydrate(),
  )
)

let persistor = null


const offlineSupportAutoSyncFilter = createWhitelistFilter(
  'issues',
  ['issues']
)

export function createPersistor(userId, callback) {
  persistor = persistStore(store, {
    storage: AsyncStorage,
    keyPrefix: 'root',
    whitelist: ['issues'],
    transforms: [
      offlineSupportEntitiesFilter,
      offlineSupportInspectionListingScreenFilter,
      offlineSupportAutoSyncFilter,
    ],
  }, callback)
}

export default store
