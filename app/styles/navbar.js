import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  containerNavbar: {
    justifyContent: 'center',
    height: Platform.OS == 'ios' ? 75 : 55,
    paddingTop: Platform.OS == 'ios' ? 20 : 0,
    alignItems: 'center',
    paddingHorizontal: 15,
    marginBottom: 20,
    backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(119, 117, 117, 0.8)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    elevation: 5,
    shadowRadius: 5,
    shadowOpacity: 0.5
  },
  textNavbar: {
    fontFamily: 'GothamRounded-Book',
    fontWeight: 'bold',
    fontSize: 20,
    color: 'rgb(40,40,40)'
  },
})
