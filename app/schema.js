import { schema } from 'normalizr'

const Inspection = new schema.Entity('inspections')

export default {
  Inspection
}
