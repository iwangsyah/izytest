import React, { Component } from 'react';
import Back from '../assets/icons/back_ic';
import Go from '../assets/icons/go_ic';


export class BackIcon extends Component {
  render () {
    return (
      <Back/>
    )
  }
}

export class GoIcon extends Component {
  render () {
    return (
      <Go/>
    )
  }
}
