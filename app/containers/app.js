import React, { Component } from 'react'
import {
  BackHandler,
  StyleSheet,
  Text,
  View,
} from 'react-native'

import { Actions, Scene, Router } from 'react-native-router-flux'

import { connect, Provider } from 'react-redux';
import StackViewStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';
import ScannerScreen from '../components/ScannerScreen'
import ScannerSuccess from '../components/ScannerSuccess'
import ListingIssues from '../components/ListingIssues'
import EditIssue from '../components/EditIssue'

import store from '../store'

const RouterWithRedux = connect()(Router)

export default class IzyTestApp extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    let index = Actions.state.index
    if (Actions.state.index == 1) {
      BackHandler.exitApp()
      return false
    }
    Actions.pop()
    return true
  }

  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scene key="root" transitionConfig={() => ({screenInterpolator: StackViewStyleInterpolator.forHorizontal})}>
            <Scene key='scannerScreen'
                component={ScannerScreen}
                title='ScannerScreen'
                panHandlers={null}
                hideNavBar={true} />
            <Scene key='scannerSuccess'
                component={ScannerSuccess}
                title='ScannerSuccess'
                panHandlers={null}
                hideNavBar={true} />
            <Scene key='listingIssues'
                component={ListingIssues}
                title='ListingIssues'
                panHandlers={null}
                hideNavBar={true} />
            <Scene key='editIssue'
                component={EditIssue}
                title='EditIssue'
                panHandlers={null}
                hideNavBar={true} />
          </Scene>
        </RouterWithRedux>
      </Provider>
    )
  }
}
